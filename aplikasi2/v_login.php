<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<title>Aplikasi web 2</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style type="text/css">
		.panel-group{
		margin-top: 10%;
	}
	</style>
</head>
<body background="bg.jpg">
	<div class="container"> <br>
		<div class="col-sm-offset-3 col-sm-6 col-md-6">
			<div class="panel-group">
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 align="center">Login Aplikasi BBB</h3></div>
					<div class="panel-body">
						<form name="loginapp1" class="form-horizontal" role="form">

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Username</label>
								</div>
								<div class="col-sm-8">
									<input type="text" name="username" class="form-control" placeholder="Input username" value=<?php echo $_SESSION['usr_apl2']; ?>>
								</div>
							</div>

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Password</label>
								</div>
								<div class="col-sm-8">
									<input type="password" name="password" class="form-control" placeholder="Masukan Password" value=<?php echo $_SESSION['pass_apl2'] ?>>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-6">

									<input type="submit" name="submit" class="btn btn-primary col-sm-4" value="Masuk">

									<input type="reset" name="cancel" class="btn btn-danger col-sm-offset-1 col-sm-4" value="Cancel">

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>