<!DOCTYPE html>
<html>
<head>
	<title>Login dengan SSO</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<style type="text/css">
	.panel-group{
		margin-top: 10%;
	}
</style>

<body background="bg.jpg">
	<div class="container"> <br>
		<div class="col-xs-offset-3 col-sm-offset-3 col-sm-6 col-md-6">
			<div class="panel-group">
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 align="center">Login SSO (Single Sign On)</h3></div>
					<div class="panel-body">
						<form name="loginsso" class="form-horizontal" role="form" action="c_login.php" method="POST">
							<?php
								$message=isset($_GET['message'])? 
												$_GET['message'] : "";
							?>
							<input type="hidden" name="message">

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Username</label>
								</div>
								<div class="col-sm-8">
									<input type="text" name="username" class="form-control" placeholder="Input username">
								</div>
							</div>

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Password</label>
								</div>
								<div class="col-sm-8">
									<input type="password" name="password" class="form-control" placeholder="Masukan Password">
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-6">

									<input type="submit" name="submit" class="btn btn-primary col-sm-4" value="Masuk">

									<input type="reset" name="cancel" class="btn btn-danger col-sm-offset-1 col-sm-4" value="Cancel">

								</div>
							</div>
							<input type="hidden" name="url" value=
								<?php echo isset($_GET['url'])?
								$_GET['url']:"" ?> >
							<label><?php echo $message; ?></label>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>