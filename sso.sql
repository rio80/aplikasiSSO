/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sso

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-03-27 11:49:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `user_sso` varchar(100) DEFAULT NULL,
  `pass_sso` varchar(100) DEFAULT NULL,
  `id_sso` int(10) NOT NULL AUTO_INCREMENT,
  `session` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_sso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES ('sso', '1234', '1', '1');
INSERT INTO `auth` VALUES ('qwe', '1234', '2', '1');

-- ----------------------------
-- Table structure for domain
-- ----------------------------
DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id_domain` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_domain`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of domain
-- ----------------------------
INSERT INTO `domain` VALUES ('1', 'http://localhost/aplikasi1', '');
INSERT INTO `domain` VALUES ('2', 'http://localhost/aplikasi2', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `id_sso` int(11) DEFAULT NULL,
  `id_domain` int(10) DEFAULT NULL,
  `apikey` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '181164', '12345678', '1', '1', '1', 'k1234rr');
INSERT INTO `user` VALUES ('2', '181164', '22222222', '2', '1', '2', 'l6543dd');
INSERT INTO `user` VALUES ('3', '111111', '12345678', '1', '2', '1', 'fe23223');
