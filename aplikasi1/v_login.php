<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head>
	<title>Aplikasi web 1</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style type="text/css">
	.panel-group{
		margin-top: 10%;
	}
</style>
<body background="bg.jpg">
	<div class="container"> <br>
		<div class="col-sm-offset-3 col-sm-6 col-md-6">
			<div class="panel-group">
				<div class="panel panel-success">
					<div class="panel-heading"><h3 align="center">Login Aplikasi AAA</h3></div>
					<div class="panel-body">
						<form name="loginapp1" class="form-horizontal" role="form" action="login.php" method="post">

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Username</label>
								</div>
								<div class="col-sm-8">
									<input type="text" name="username" class="form-control" placeholder="Input username" value=<?php echo isset($_SESSION['usr_apl1'])?$_SESSION['usr_apl1']:""; ?>>
								</div>
							</div>

							<div class="form-group">
								<div class="control-label col-sm-4">
									<label>Masukan Password</label>
								</div>
								<div class="col-sm-8">
									<input type="password" name="password" class="form-control" placeholder="Masukan Password" value=<?php echo isset($_SESSION['pass_apl1'])?$_SESSION['pass_apl1']:""; ?>>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-6">

									<input type="submit" name="submit" class="btn btn-primary col-sm-4" value="Masuk">

									<input type="reset" name="cancel" class="btn btn-danger col-sm-offset-1 col-sm-5" value="Cancel">

								</div>
							</div>
						</form>


						<form name="formsso" class="form-horizontal" role="form" action="../sso/index.php" method="post">
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-6">
									<input type="hidden" name="urlapl" id="urlapl" value=<?php
									echo 'http://'.$_SERVER['HTTP_HOST'].'/aplikasi1'; ?> >
									<input type="submit" name="loginsso" class="btn btn-primary col-sm-10" value="Login With SSO">
								</div>
							</div>
						</form>

						<?php
						// echo 'http://'.$_SERVER['HTTP_HOST'].
						// $_SERVER['PHP_SELF'];
						echo 'http://'.$_SERVER['HTTP_HOST'].'/aplikasi1';

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>